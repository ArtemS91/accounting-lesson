import { useContext, useState } from "react";
import {SumContext} from "../../context/sumContext";

export default function Header() {
  const [theme, setTheme] = useState('light');
  const {sum} = useContext(SumContext);
  console.log(sum);
  return (<header style={{display: 'flex', justifyContent: 'space-between'}}>
    <span className="header__title">Tabler</span>
    <button onClick={
      () => setTheme((prev)=> prev === 'light' ? 'dark' : 'light')}>toggle theme</button>
    <span data-testid='theme'>current theme: {theme}</span>
    <div>
      <span data-testid='header-context'>{sum >= 0 ? `$${sum}` : `-$${Math.abs(sum)}`}</span>
      <span>A</span>
    </div>
  </header>)
}