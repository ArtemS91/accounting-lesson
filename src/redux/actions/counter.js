import {counterTypes} from "../types";

export function resetCounter() {
    return {
        type: counterTypes.reset
    }
}

export function increaseCounter() {
    return {
        type: counterTypes.increase
    }
}

export function decreaseCounter() {
    return {
        type: counterTypes.decrease
    }
}