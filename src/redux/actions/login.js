import {loginTypes} from "../types";

function login(response) {
    return {
        type: loginTypes.login,
        payload: response
    }
}

export function loginAsync(name) {
    return async function(dispatch) {
        const { token } = await fetch('https://accounting-be.vercel.app/login', {
            method: "POST",
            headers: {
                ContentType: 'application/text',
            },
            body: name
        }).then(res=>res.json());
        localStorage.setItem('token', token);
        dispatch(login(token));
    }
}