import {createContext} from "react";

export const SumContext = createContext(null)

export const SumDispatchContext = createContext(null)